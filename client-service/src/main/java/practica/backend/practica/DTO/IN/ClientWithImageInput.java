package practica.backend.practica.DTO.IN;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import practica.backend.practica.entities.Cliente;
import practica.backend.practica.entities.ClienteId;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Positive;
import java.io.Serializable;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClientWithImageInput implements Serializable {


    @NotEmpty(message = "El tipo de identificación no puede ser vacío")
    private String tipo_id;
    @Positive(message = "La identificación debe ser mayor que cero")
    private int num_id;
    @NotEmpty(message = "Los nombres no pueden ser vacíos")
    private String nombres;
    @NotEmpty(message = "Los apellidos no pueden ser vacíos")
    private String apellidos;
    @Positive(message = "La edad debe ser mayor que cero")
    private int edad;
    @NotEmpty(message = "La ciudad no puede ser vacía")
    private String ciudad;
    @NotEmpty(message = "Campo de imágen no puede ser vacio")
    private String image64;

    public Cliente toCliente(){
        return Cliente.builder()
                .clienteId(ClienteId.builder()
                        .tipo_id(getTipo_id())
                        .num_id(getNum_id()).build())
                .nombres(getNombres())
                .apellidos(getApellidos())
                .edad(getEdad())
                .ciudad(getCiudad()).build();
    }


}
