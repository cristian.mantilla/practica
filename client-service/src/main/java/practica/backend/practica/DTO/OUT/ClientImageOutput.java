package practica.backend.practica.DTO.OUT;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import practica.backend.practica.model.ClientId;
import practica.backend.practica.model.ClientImage;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ClientImageOutput implements Serializable {

    private ClientId clientId;
    private String image64;

    public ClientImage toClientImage(){
        return ClientImage.builder()
                .clientId(ClientId.builder()
                        .tipo_id(getClientId().getTipo_id())
                        .num_id(getClientId().getNum_id()).build())
                .image64(getImage64()).build();
    }

}
