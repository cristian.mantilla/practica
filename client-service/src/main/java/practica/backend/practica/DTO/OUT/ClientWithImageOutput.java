package practica.backend.practica.DTO.OUT;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import practica.backend.practica.entities.Cliente;
import practica.backend.practica.entities.ClienteId;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClientWithImageOutput implements Serializable {


    private String tipo_id;
    private int num_id;
    private String nombres;
    private String apellidos;
    private int edad;
    private String ciudad;
    private String image64;

    public Cliente toCliente(){
        return Cliente.builder()
                .clienteId(ClienteId.builder()
                        .tipo_id(getTipo_id())
                        .num_id(getNum_id()).build())
                .nombres(getNombres())
                .apellidos(getApellidos())
                .edad(getEdad())
                .ciudad(getCiudad()).build();
    }


}
