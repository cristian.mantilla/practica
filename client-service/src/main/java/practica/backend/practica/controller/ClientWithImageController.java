package practica.backend.practica.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import practica.backend.practica.DTO.IN.ClienteInput;
import practica.backend.practica.DTO.OUT.ClientWithImageOutput;
import practica.backend.practica.entities.ClienteId;
import practica.backend.practica.service.ClientWithImageService;

import javax.validation.Valid;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping (value = "/client-service/clientsWithImages")
public class ClientWithImageController {

    @Autowired
    private ClientWithImageService clientWithImageService;

    @GetMapping
    public ResponseEntity<List<ClientWithImageOutput>> listCliente(@RequestParam(name = "edad", required = false) Integer edad){
        List<ClientWithImageOutput> clientes = new ArrayList<>();

        if (null == edad){
            clientes = clientWithImageService.listAllCliente();
            if(clientes.isEmpty()){
                return ResponseEntity.noContent().build();
            }
        }
        else{
            clientes = clientWithImageService.findAllByEdad(edad);
            if(clientes.isEmpty()){
                return ResponseEntity.noContent().build();
            }
        }
        return ResponseEntity.ok(clientes);
    }

    @GetMapping(value = "/{tipo}-{id}")
    public ResponseEntity<ClientWithImageOutput> getCliente
            (@PathVariable("tipo") String tipo,@PathVariable("id") int id){

        ClienteId clienteId = ClienteId.builder()
                .num_id(id)
                .tipo_id(tipo).build();
        log.info("Fetching Cliente with id {}", clienteId);
        ClientWithImageOutput cliente = clientWithImageService.getCliente(clienteId);

        if(null == cliente){
            log.error("Cliente with id {} not found.", clienteId);
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(cliente);

    }

    @PostMapping
    public ResponseEntity<ClientWithImageOutput> createCliente(@Valid @RequestPart ClienteInput clienteInput
                    , @RequestPart MultipartFile multipartFile, BindingResult result) throws IOException {

        log.info("Creating Cliente : {}", clienteInput);
        if(result.hasErrors()){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, this.formatMessage(result));
        }

        ClientWithImageOutput cliente1 = clientWithImageService.createCliente(clienteInput, multipartFile);
        return ResponseEntity.status(HttpStatus.CREATED).body(cliente1);

    }

    @PutMapping()
    public ResponseEntity<ClientWithImageOutput> updateCliente(@Valid @RequestPart ClienteInput clienteInput
            , @RequestPart MultipartFile multipartFile, BindingResult result) throws IOException {

        log.info("Creating Cliente : {}", clienteInput);
        if(result.hasErrors()){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, this.formatMessage(result));
        }

        ClienteId clienteId = ClienteId.builder()
                .num_id(clienteInput.getNum_id())
                .tipo_id(clienteInput.getTipo_id()).build();

        log.info("Updating Cliente with id {}", clienteId);
        clienteInput.setTipo_id(clienteId.getTipo_id());
        clienteInput.setNum_id(clienteId.getNum_id());
        ClientWithImageOutput cliente1 = clientWithImageService.updateCliente(clienteInput, multipartFile);

        if(cliente1 == null){
            log.error("Unable to update. Cliente with id {} not found.", clienteId);
            return ResponseEntity.noContent().build();
        }

        return ResponseEntity.ok(cliente1);
    }

    @DeleteMapping(value = "/{tipo}-{id}")
    public ResponseEntity<ClientWithImageOutput> deleteCliente(@PathVariable("tipo") String tipo,@PathVariable("id") int id){

        ClienteId clienteId = ClienteId.builder().num_id(id).tipo_id(tipo).build();
        log.info("Fetching & Deleting Cliente with id {}", clienteId);
        ClientWithImageOutput cliente1 = clientWithImageService.deleteCliente(clienteId);

        if(cliente1 == null){
            log.error("Unable to delete. Cliente with id {} not found.", clienteId);
            return ResponseEntity.noContent().build();
        }

        return ResponseEntity.ok(cliente1);
    }

    private String formatMessage( BindingResult result){
        List<Map<String,String>> errors = result.getFieldErrors().stream()
                .map(err -> {
                    Map<String,String> error = new HashMap<>();
                    error.put(err.getField(),err.getDefaultMessage());
                    return error;
                }).collect(Collectors.toList());

        ErrorMessage errorMessage = ErrorMessage.builder().code("01").messages(errors).build();

        //Transformar mensaje de error a Json
        ObjectMapper mapper = new ObjectMapper();
        String jsonString="";
        try {
            jsonString = mapper.writeValueAsString(errorMessage);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return jsonString;
    }

}
