package practica.backend.practica.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import practica.backend.practica.DTO.IN.ClienteInput;
import practica.backend.practica.DTO.OUT.ClienteOutput;
import practica.backend.practica.entities.Cliente;
import practica.backend.practica.entities.ClienteId;
import practica.backend.practica.service.ClienteService;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping (value = "/client-service/clientes")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @GetMapping
    public ResponseEntity<List<ClienteOutput>> listCliente(@RequestParam(name = "edad", required = false) Integer edad){
        List<ClienteOutput> clientes = new ArrayList<>();

        if (null == edad){
            clientes = clienteService.listAllCliente();
            if(clientes.isEmpty()){
                return ResponseEntity.noContent().build();
            }
        }
        else{
            clientes = clienteService.findAllByEdad(edad);
            if(clientes.isEmpty()){
                return ResponseEntity.noContent().build();
            }
        }
        return ResponseEntity.ok(clientes);
    }

    @GetMapping(value = "/{tipo}-{id}")
    public ResponseEntity<ClienteOutput> getCliente(@PathVariable("tipo") String tipo,@PathVariable("id") int id){
        ClienteId clienteId = ClienteId.builder()
                .num_id(id)
                .tipo_id(tipo).build();
        log.info("Fetching Cliente with id {}", clienteId);
        ClienteOutput cliente = clienteService.getCliente(clienteId);

        if(null == cliente){
            log.error("Cliente with id {} not found.", clienteId);
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(cliente);

    }

    @PostMapping
    public ResponseEntity<ClienteOutput> createCliente(@Valid @RequestBody ClienteInput clienteInput, BindingResult result){

        log.info("Creating Cliente : {}", clienteInput);
        if(result.hasErrors()){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, this.formatMessage(result));
        }

        ClienteOutput cliente1 = clienteService.createCliente(clienteInput);
        return ResponseEntity.status(HttpStatus.CREATED).body(cliente1);

    }

    @PutMapping()
    public ResponseEntity<ClienteOutput> updateCliente(@Valid @RequestBody ClienteInput clienteInput){

        ClienteId clienteId = ClienteId.builder()
                .num_id(clienteInput.getNum_id())
                .tipo_id(clienteInput.getTipo_id()).build();

        log.info("Updating Cliente with id {}", clienteId);
        clienteInput.setTipo_id(clienteId.getTipo_id());
        clienteInput.setNum_id(clienteId.getNum_id());
        ClienteOutput cliente1 = clienteService.updateCliente(clienteInput);

        if(cliente1 == null){
            log.error("Unable to update. Cliente with id {} not found.", clienteId);
            return ResponseEntity.noContent().build();
        }

        return ResponseEntity.ok(cliente1);
    }

    @DeleteMapping(value = "/{tipo}-{id}")
    public ResponseEntity<ClienteOutput> deleteCliente(@PathVariable("tipo") String tipo,@PathVariable("id") int id){

        ClienteId clienteId = ClienteId.builder().num_id(id).tipo_id(tipo).build();
        log.info("Fetching & Deleting Cliente with id {}", clienteId);
        ClienteOutput cliente1 = clienteService.deleteCliente(clienteId);

        if(cliente1 == null){
            log.error("Unable to delete. Cliente with id {} not found.", clienteId);
            return ResponseEntity.noContent().build();
        }

        return ResponseEntity.ok(cliente1);
    }

    private String formatMessage( BindingResult result){
        List<Map<String,String>> errors = result.getFieldErrors().stream()
                .map(err -> {
                    Map<String,String> error = new HashMap<>();
                    error.put(err.getField(),err.getDefaultMessage());
                    return error;
                }).collect(Collectors.toList());

        ErrorMessage errorMessage = ErrorMessage.builder().code("01").messages(errors).build();

        //Transformar mensaje de error a Json
        ObjectMapper mapper = new ObjectMapper();
        String jsonString="";
        try {
            jsonString = mapper.writeValueAsString(errorMessage);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return jsonString;
    }

}
