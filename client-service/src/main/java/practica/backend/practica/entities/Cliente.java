package practica.backend.practica.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import practica.backend.practica.DTO.IN.ClienteInput;
import practica.backend.practica.DTO.OUT.ClientWithImageOutput;
import practica.backend.practica.DTO.OUT.ClienteOutput;
import practica.backend.practica.model.ClientImage;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Positive;

@Entity
@Table(name = "cliente")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
//@SQLDelete(sql = "UPDATE cliente SET actived = NULL WHERE id=?")
//@Where(clause = "actived=true")
public class Cliente {

    @EmbeddedId
    private ClienteId clienteId;
    private String nombres;
    private String apellidos;
    private int edad;
    private String ciudad;

    @Transient
    private ClientImage clientImage;

    public ClienteInput toInput(){
        return ClienteInput.builder()
                .tipo_id(getClienteId().getTipo_id())
                .num_id(getClienteId().getNum_id())
                .nombres(getNombres())
                .apellidos(getApellidos())
                .edad(getEdad())
                .ciudad(getCiudad()).build();
    }

    public ClienteOutput toOutput(){
        return ClienteOutput.builder()
                .tipo_id(getClienteId().getTipo_id())
                .num_id(getClienteId().getNum_id())
                .nombres(getNombres())
                .apellidos(getApellidos())
                .edad(getEdad())
                .ciudad(getCiudad()).build();
    }

    public ClientWithImageOutput toImageOutput(){
        return ClientWithImageOutput.builder()
                .tipo_id(getClienteId().getTipo_id())
                .num_id(getClienteId().getNum_id())
                .nombres(getNombres())
                .apellidos(getApellidos())
                .edad(getEdad())
                .ciudad(getCiudad())
                .image64("").build();
    }

}
