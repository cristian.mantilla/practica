package practica.backend.practica.entities;

import lombok.*;
import practica.backend.practica.model.ClientId;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Id;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Embeddable
@Builder
public class ClienteId implements Serializable {

    private String tipo_id;
    private int num_id;

    public ClientId toClientId (){
        return ClientId.builder().num_id(num_id).tipo_id(tipo_id).build();
    }

}
