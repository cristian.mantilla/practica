package practica.backend.practica.feignClient;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import practica.backend.practica.DTO.IN.ClientImageInput;
import practica.backend.practica.DTO.OUT.ClientImageOutput;
import practica.backend.practica.entities.ClienteId;
import practica.backend.practica.model.ClientId;
import practica.backend.practica.model.ClientImage;

import java.util.List;

@FeignClient(name = "image-service", fallback = ClientImageHystrixFallbackFactory.class)
public interface ClientImageFeign {

    @GetMapping("/images")
    public ResponseEntity<List<ClientImageOutput>> listClientImage ();

    @GetMapping(value = "/images/{tipo}-{id}")
    public ResponseEntity<ClientImageOutput> getClientImage
            (@PathVariable("tipo") String tipo, @PathVariable("id") int id);

    @PostMapping("/images")
    public ResponseEntity<ClientImageOutput> createClientImage
            (@RequestBody ClientImageInput clientImageInput);

    @PostMapping("/images/multiple")
    public ResponseEntity<List<ClientImageOutput>> getMultipleClientImage(@RequestBody List<ClientId> clientsId);

    @PutMapping("/images")
    public ResponseEntity<ClientImageOutput> updateClientImage
            (@RequestBody ClientImageInput clientImageInput);

    @DeleteMapping(value = "/images/{tipo}-{id}")
    public ResponseEntity<ClientImageOutput> deleteClientImage
            (@PathVariable("tipo") String tipo,@PathVariable("id") int id);

}
