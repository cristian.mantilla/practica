package practica.backend.practica.feignClient;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import practica.backend.practica.DTO.IN.ClientImageInput;
import practica.backend.practica.DTO.OUT.ClientImageOutput;
import practica.backend.practica.entities.ClienteId;
import practica.backend.practica.model.ClientId;

import java.util.List;

@Component
public class ClientImageHystrixFallbackFactory implements  ClientImageFeign{


    @Override
    public ResponseEntity<List<ClientImageOutput>> listClientImage() {
        return null;
    }

    @Override
    public ResponseEntity<ClientImageOutput> getClientImage(String tipo, int id) {

        ClientImageOutput clientImageOutput = ClientImageOutput.builder().clientId(null).image64("Not Found").build();
        return ResponseEntity.ok(clientImageOutput);
    }

    @Override
    public ResponseEntity<ClientImageOutput> createClientImage(ClientImageInput clientImageInput) {
        ClientImageOutput clientImageOutput = ClientImageOutput.builder().clientId(null).image64("Not Found").build();
        return ResponseEntity.ok(clientImageOutput);
    }

    @Override
    public ResponseEntity<List<ClientImageOutput>> getMultipleClientImage(List<ClientId> clientsId) {
        return null;
    }

    @Override
    public ResponseEntity<ClientImageOutput> updateClientImage(ClientImageInput clientImageInput) {
        ClientImageOutput clientImageOutput = ClientImageOutput.builder().clientId(null).image64("Not Found").build();
        return ResponseEntity.ok(clientImageOutput);
    }

    @Override
    public ResponseEntity<ClientImageOutput> deleteClientImage(String tipo, int id) {
        ClientImageOutput clientImageOutput = ClientImageOutput.builder().clientId(null).image64("Not Found").build();
        return ResponseEntity.ok(clientImageOutput);
    }
}
