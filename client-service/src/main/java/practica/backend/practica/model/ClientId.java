package practica.backend.practica.model;

import lombok.*;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Builder
public class ClientId implements Serializable {

    private String tipo_id;
    private int num_id;

}
