package practica.backend.practica.model;

import lombok.Builder;
import lombok.Data;
import practica.backend.practica.entities.ClienteId;

@Data
@Builder
public class ClientImage {

    private ClientId clientId;
    private String image64;

}
