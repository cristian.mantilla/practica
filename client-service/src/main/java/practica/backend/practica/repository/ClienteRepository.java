package practica.backend.practica.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import practica.backend.practica.entities.Cliente;
import practica.backend.practica.entities.ClienteId;

import java.util.List;

@Repository
public interface ClienteRepository extends JpaRepository<Cliente, ClienteId> {
    @Query("FROM Cliente WHERE edad >= ?1")
    public List<Cliente> findAllByEdad(int edad);
}
