package practica.backend.practica.service;

import org.springframework.web.multipart.MultipartFile;
import practica.backend.practica.DTO.IN.ClientWithImageInput;
import practica.backend.practica.DTO.IN.ClienteInput;
import practica.backend.practica.DTO.OUT.ClientWithImageOutput;
import practica.backend.practica.DTO.OUT.ClienteOutput;
import practica.backend.practica.entities.ClienteId;

import java.io.IOException;
import java.util.List;

public interface ClientWithImageService {

    public List<ClientWithImageOutput> listAllCliente();
    public ClientWithImageOutput getCliente(ClienteId clienteId);
    public ClientWithImageOutput createCliente(ClienteInput clienteInput, MultipartFile multipartFile) throws IOException;
    public ClientWithImageOutput updateCliente(ClienteInput clienteInput, MultipartFile multipartFile) throws IOException;
    public ClientWithImageOutput deleteCliente(ClienteId clienteId);
    public List<ClientWithImageOutput> findAllByEdad(int edad);
}
