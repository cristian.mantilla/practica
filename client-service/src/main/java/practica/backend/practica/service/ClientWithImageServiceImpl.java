package practica.backend.practica.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import practica.backend.practica.DTO.IN.ClientImageInput;
import practica.backend.practica.DTO.IN.ClientWithImageInput;
import practica.backend.practica.DTO.IN.ClienteInput;
import practica.backend.practica.DTO.OUT.ClientImageOutput;
import practica.backend.practica.DTO.OUT.ClientWithImageOutput;
import practica.backend.practica.DTO.OUT.ClienteOutput;
import practica.backend.practica.entities.Cliente;
import practica.backend.practica.entities.ClienteId;
import practica.backend.practica.feignClient.ClientImageFeign;
import practica.backend.practica.model.ClientId;
import practica.backend.practica.model.ClientImage;
import practica.backend.practica.repository.ClienteRepository;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ClientWithImageServiceImpl implements ClientWithImageService {

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    ClientImageFeign clientImageFeign;

    @Override
    public List<ClientWithImageOutput> listAllCliente() {
        List<ClientWithImageOutput> clientes = new ArrayList<>();
        List<ClienteId> clienteIds = clienteRepository.findAll()
                .stream()
                .map(Cliente::getClienteId)
                .collect(Collectors.toList());

        List<ClientImage> images = clientImageFeign.getMultipleClientImage(
                clienteIds.stream().map(ClienteId::toClientId).collect(Collectors.toList()))
                .getBody().stream().map(ClientImageOutput::toClientImage).collect(Collectors.toList());

        Map<ClientId, String> map = images.stream()
                .collect(Collectors.toMap(ClientImage::getClientId, ClientImage::getImage64));

        for (Cliente cliente: clienteRepository.findAll()){
            ClientWithImageOutput clientWithImageOutput = cliente.toImageOutput();
            clientWithImageOutput.setImage64(map.getOrDefault(cliente.getClienteId().toClientId(), "Not Found"));
            clientes.add(clientWithImageOutput);
        }

        return clientes;
    }

    @Override
    public ClientWithImageOutput getCliente(ClienteId clienteId) {
        Optional<Cliente> clienteOptional = clienteRepository.findById(clienteId);
        if (clienteOptional.isPresent()){
            return toClientWithImageOutput(clienteOptional.get());
        }
        else{
            return null;
        }
    }

    @Override
    public ClientWithImageOutput createCliente
            (ClienteInput clienteInput, MultipartFile multipartFile) throws IOException {
        ClientWithImageOutput clientWithImageOutput = clienteRepository.save(clienteInput.toCliente()).toImageOutput();

        ClientImageInput clientImageInput = ClientImageInput.builder()
                .clientId(ClientId.builder()
                        .tipo_id(clienteInput.getTipo_id())
                        .num_id(clienteInput.getNum_id()).build())
                .image64(Base64.getEncoder().encodeToString(multipartFile.getBytes())).build();

        ResponseEntity<ClientImageOutput> clientImageOutputResponseEntity = clientImageFeign.createClientImage(clientImageInput);
        ClientImageOutput clientImageOutput = clientImageOutputResponseEntity.getBody();

        clientWithImageOutput.setImage64(clientImageOutput.getImage64());

        return clientWithImageOutput;
    }

    @Override
    public ClientWithImageOutput updateCliente
            (ClienteInput clienteInput, MultipartFile multipartFile) throws IOException {
        ClientWithImageOutput foundCliente = getCliente(clienteInput.toCliente().getClienteId());

        if (null == foundCliente){
            return null;
        }
        Cliente cliente1 = foundCliente.toCliente();
        cliente1.setClienteId
                (ClienteId.builder().tipo_id(clienteInput.getTipo_id()).num_id(clienteInput.getNum_id()).build());
        cliente1.setNombres(clienteInput.getNombres());
        cliente1.setApellidos(clienteInput.getApellidos());
        cliente1.setEdad(clienteInput.getEdad());
        cliente1.setCiudad(clienteInput.getCiudad());

        ClientWithImageOutput clientWithImageOutput = clienteRepository.save(cliente1).toImageOutput();
        ClientImageOutput clientImageOutput = clientImageFeign.updateClientImage(ClientImageInput.builder()
                .clientId(ClientId.builder()
                        .tipo_id(clienteInput.getTipo_id())
                        .num_id(clienteInput.getNum_id()).build())
                .image64(Base64.getEncoder().encodeToString(multipartFile.getBytes())).build()).getBody();

        clientWithImageOutput.setImage64(clientImageOutput.getImage64());

        return clientWithImageOutput;
    }

    @Override
    public ClientWithImageOutput deleteCliente(ClienteId clienteId) {

        ClientWithImageOutput foundCliente = getCliente(clienteId);

        if (null == foundCliente){
            return null;
        }
        Cliente cliente1 = foundCliente.toCliente();
        clienteRepository.delete(cliente1);

        ClientWithImageOutput clientWithImageOutput = cliente1.toImageOutput();
        ClientImageOutput clientImageOutput =
                clientImageFeign.deleteClientImage(clienteId.getTipo_id(),clienteId.getNum_id()).getBody();
        clientWithImageOutput.setImage64(clientImageOutput.getImage64());

        return clientWithImageOutput;
    }

    @Override
    public List<ClientWithImageOutput> findAllByEdad(int edad) {
        List<ClientWithImageOutput> clientes = new ArrayList<>();
        List<ClienteId> clienteIds = clienteRepository.findAllByEdad(edad)
                .stream()
                .map(Cliente::getClienteId)
                .collect(Collectors.toList());

        List<ClientImage> images = clientImageFeign.getMultipleClientImage(
                clienteIds.stream().map(ClienteId::toClientId).collect(Collectors.toList()))
                .getBody().stream().map(ClientImageOutput::toClientImage).collect(Collectors.toList());

        Map<ClientId, String> map = images.stream()
                .collect(Collectors.toMap(ClientImage::getClientId, ClientImage::getImage64));

        for (Cliente cliente: clienteRepository.findAllByEdad(edad)){
            ClientWithImageOutput clientWithImageOutput = cliente.toImageOutput();
            clientWithImageOutput.setImage64(map.getOrDefault(cliente.getClienteId().toClientId(), "Not Found"));
            clientes.add(clientWithImageOutput);
        }

        return clientes;
    }

    public List<ClientWithImageOutput> toOutputList(List<Cliente> clientes){
        List<ClientWithImageOutput>  clientWithImageOutputs = new ArrayList<>();
        for(Cliente cliente : clientes) {
            clientWithImageOutputs.add(toClientWithImageOutput(cliente));
        }
        return clientWithImageOutputs;
    }

    public ClientWithImageOutput toClientWithImageOutput(Cliente cliente){
        ClientWithImageOutput clientWithImageOutput = cliente.toImageOutput();
        clientWithImageOutput.setImage64(clientImageFeign.getClientImage(clientWithImageOutput.getTipo_id()
                , clientWithImageOutput.getNum_id()).getBody().getImage64());
        return clientWithImageOutput;
    }
}
