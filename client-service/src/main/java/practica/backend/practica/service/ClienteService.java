package practica.backend.practica.service;

import practica.backend.practica.DTO.IN.ClienteInput;
import practica.backend.practica.DTO.OUT.ClienteOutput;
import practica.backend.practica.entities.Cliente;
import practica.backend.practica.entities.ClienteId;

import java.util.List;

public interface ClienteService {

    public List<ClienteOutput> listAllCliente();
    public ClienteOutput getCliente(ClienteId clienteId);
    public ClienteOutput createCliente(ClienteInput clienteInput);
    public ClienteOutput updateCliente(ClienteInput clienteInput);
    public ClienteOutput deleteCliente(ClienteId clienteId);
    public List<ClienteOutput> findAllByEdad(int edad);

}
