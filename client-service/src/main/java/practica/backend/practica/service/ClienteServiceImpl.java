package practica.backend.practica.service;

import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import practica.backend.practica.DTO.IN.ClienteInput;
import practica.backend.practica.DTO.OUT.ClienteOutput;
import practica.backend.practica.entities.Cliente;
import practica.backend.practica.entities.ClienteId;
import practica.backend.practica.feignClient.ClientImageFeign;
import practica.backend.practica.repository.ClienteRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ClienteServiceImpl implements  ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    @Override
    public List<ClienteOutput> listAllCliente() {
        return toOutputList(clienteRepository.findAll());
    }

    @Override
    public ClienteOutput getCliente(ClienteId clienteId) {
        Optional<Cliente> clienteOptional = clienteRepository.findById(clienteId);
        if (clienteOptional.isPresent()){
            return clienteOptional.get().toOutput();
        }
        else{
            return null;
        }
    }

    @Override
    public ClienteOutput createCliente(ClienteInput clienteInput) {
        return clienteRepository.save(clienteInput.toCliente()).toOutput();
    }

    @Override
    public ClienteOutput updateCliente(ClienteInput clienteInput) {
        ClienteOutput foundCliente = getCliente(clienteInput.toCliente().getClienteId());

        if (null == foundCliente){
            return null;
        }
        Cliente cliente1 = foundCliente.toCliente();
        cliente1.setClienteId
                (ClienteId.builder().tipo_id(clienteInput.getTipo_id()).num_id(clienteInput.getNum_id()).build());
        cliente1.setNombres(clienteInput.getNombres());
        cliente1.setApellidos(clienteInput.getApellidos());
        cliente1.setEdad(clienteInput.getEdad());
        cliente1.setCiudad(clienteInput.getCiudad());

        return clienteRepository.save(cliente1).toOutput();
    }

    @Override
    public ClienteOutput deleteCliente(ClienteId clienteId) {
        ClienteOutput foundCliente = getCliente(clienteId);

        if (null == foundCliente){
            return null;
        }
        Cliente cliente1 = foundCliente.toCliente();
        clienteRepository.delete(cliente1);
        return cliente1.toOutput();
    }

    @Override
    public List<ClienteOutput> findAllByEdad(int edad) {
        return toOutputList(clienteRepository.findAllByEdad(edad));
    }


    public List<ClienteOutput> toOutputList(List<Cliente> clientes){
        List<ClienteOutput>  clientesOut = new ArrayList<>();
        for(Cliente cliente : clientes) {
            clientesOut.add(cliente.toOutput());
        }
        return clientesOut;
    }
}
