package practica.backend.practica.controller;

import static  org.mockito.Mockito.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

import org.springframework.test.web.servlet.request.MockMultipartHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.request.RequestPostProcessor;
import org.springframework.web.multipart.MultipartFile;
import practica.backend.practica.DTO.IN.ClientWithImageInput;
import practica.backend.practica.DTO.IN.ClienteInput;
import practica.backend.practica.DTO.OUT.ClientWithImageOutput;
import practica.backend.practica.DTO.OUT.ClienteOutput;
import practica.backend.practica.entities.Cliente;
import practica.backend.practica.entities.ClienteId;
import practica.backend.practica.model.ClientImage;
import practica.backend.practica.service.ClientWithImageServiceImpl;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(ClientWithImageController.class)
class ClientWithImageControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ClientWithImageServiceImpl clientWithImageService;

    ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        objectMapper = new ObjectMapper();
    }

    @MockBean
    Cliente cliente;

    @Mock
    MultipartFile multipartFile;

    @Test
    void listClienteTest() throws Exception {
        List<ClientWithImageOutput> clientes = new ArrayList<>();
        clientes.add(cliente.toImageOutput());
        when(clientWithImageService.listAllCliente()).thenReturn(clientes);

        mockMvc.perform(get("/client-service/clientsWithImages").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    void listClienteEdadTest() throws Exception {
        List<ClientWithImageOutput> clientes = new ArrayList<>();

        ClientWithImageOutput cliente1 = ClientWithImageOutput.builder().num_id(123).tipo_id("Cedula")
                .nombres("AA")
                .apellidos("AA")
                .edad(20)
                .ciudad("AA").image64("Not Found").build();
        ClientWithImageOutput cliente2 = ClientWithImageOutput.builder().num_id(321).tipo_id("Cedula")
                .nombres("AA")
                .apellidos("AA")
                .edad(30)
                .ciudad("AA").image64("Not Found").build();

        clientes.add(cliente1);
        clientes.add(cliente2);
        when(clientWithImageService.listAllCliente()).thenReturn(clientes);
        when(clientWithImageService.findAllByEdad(25)).thenReturn(clientes);

        mockMvc.perform(get("/client-service/clientsWithImages?edad=25").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    void getClienteTest() throws Exception {
        ClientWithImageOutput cliente1 = ClientWithImageOutput.builder().num_id(123).tipo_id("Cedula")
                .nombres("AA")
                .apellidos("AA")
                .edad(20)
                .ciudad("AA").image64("Not Found").build();

        when(clientWithImageService.getCliente(ClienteId.builder()
                .num_id(123)
                .tipo_id("Cedula").build())).thenReturn(cliente1);

        mockMvc.perform(get("/client-service/clientsWithImages/Cedula-123").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.nombres").value(cliente1.getNombres()));
    }

    @Test
    void getClienteNullTest() throws Exception {
        when(clientWithImageService.getCliente(ClienteId.builder()
                .num_id(123)
                .tipo_id("Cedula").build())).thenReturn(null);

        mockMvc.perform(get("/client-service/clientsWithImages/Cedula-123").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    @Test
    void createClienteTest() throws Exception {
        ClienteInput clienteInput = ClienteInput.builder().num_id(123).tipo_id("Cedula")
                .nombres("AA")
                .apellidos("AA")
                .edad(20)
                .ciudad("AA").build();

        MockMultipartFile file1 = new MockMultipartFile("clienteInput", "filename-1.json"
                , "application/json",objectMapper.writeValueAsBytes(clienteInput));
        MockMultipartFile file2 = new MockMultipartFile("multipartFile"
                , "filename-2.txt", "text/plain", "1".getBytes());


        when(clientWithImageService.createCliente(clienteInput,multipartFile)).thenReturn(clienteInput.toCliente().toImageOutput());

        mockMvc.perform(MockMvcRequestBuilders.multipart("/client-service/clientsWithImages")
                .file(file1)
                .file(file2)
                .contentType(MediaType.MULTIPART_FORM_DATA))
                .andExpect(status().isCreated());
    }

    @Test
    void createClienteBadRequestTest() throws Exception {
        ClienteInput clienteInput = ClienteInput.builder().num_id(123).tipo_id("Cedula")
                .nombres("AA")
                .apellidos("AA")
                .edad(0) // Edad no puede ser igual o menor a cero
                .ciudad("AA").build();

        MockMultipartFile file1 = new MockMultipartFile("clienteInput", "filename-1.json"
                , "application/json",objectMapper.writeValueAsBytes(clienteInput));
        MockMultipartFile file2 = new MockMultipartFile("multipartFile"
                , "filename-2.txt", "text/plain", "1".getBytes());

        mockMvc.perform(MockMvcRequestBuilders.multipart("/client-service/clientsWithImages")
                .file(file1)
                .file(file2)
                .contentType(MediaType.MULTIPART_FORM_DATA))
                .andExpect(status().isBadRequest());
    }

    @Test
    void updateClienteTest() throws Exception {
        ClienteInput clienteInput = ClienteInput.builder().num_id(123).tipo_id("Cedula")
                .nombres("AA")
                .apellidos("AA")
                .edad(20)
                .ciudad("AA").build();

        MockMultipartFile file1 = new MockMultipartFile("clienteInput", "filename-1.json"
                , "application/json",objectMapper.writeValueAsBytes(clienteInput));
        MockMultipartFile file2 = new MockMultipartFile("multipartFile"
                , "filename-2.txt", "text/plain", "1".getBytes());

        MockMultipartHttpServletRequestBuilder builder =
                MockMvcRequestBuilders.multipart("/client-service/clientsWithImages");

        builder.with(new RequestPostProcessor() {
            @Override
            public MockHttpServletRequest postProcessRequest(MockHttpServletRequest request) {
                request.setMethod("PUT");
                return request;
            }
        });

        when(clientWithImageService.updateCliente(any(), any()))
                .thenReturn(clienteInput.toCliente().toImageOutput());

        mockMvc.perform(builder
                .file(file1)
                .file(file2)
                .contentType(MediaType.MULTIPART_FORM_DATA))
                .andExpect(status().isOk());
    }

    @Test
    void updateClienteNullTest() throws Exception {
        ClienteInput clienteInput = ClienteInput.builder().num_id(123).tipo_id("Cedula")
                .nombres("AA")
                .apellidos("AA")
                .edad(20)
                .ciudad("AA").build();

        MockMultipartFile file1 = new MockMultipartFile("clienteInput", "filename-1.json"
                , "application/json",objectMapper.writeValueAsBytes(clienteInput));
        MockMultipartFile file2 = new MockMultipartFile("multipartFile"
                , "filename-2.txt", "text/plain", "1".getBytes());

        MockMultipartHttpServletRequestBuilder builder =
                MockMvcRequestBuilders.multipart("/client-service/clientsWithImages");

        builder.with(new RequestPostProcessor() {
            @Override
            public MockHttpServletRequest postProcessRequest(MockHttpServletRequest request) {
                request.setMethod("PUT");
                return request;
            }
        });

        when(clientWithImageService.updateCliente(any(),any())).thenReturn(null);

        mockMvc.perform(builder
                .file(file1)
                .file(file2)
                .contentType(MediaType.MULTIPART_FORM_DATA))
                .andExpect(status().isNoContent());
    }

    @Test
    void deleteClienteTest() throws Exception {
        ClientWithImageOutput cliente1 = ClientWithImageOutput.builder().num_id(123).tipo_id("Cedula")
                .nombres("AA")
                .apellidos("AA")
                .edad(20)
                .ciudad("AA").image64("Not Found").build();

        when(clientWithImageService.deleteCliente(ClienteId.builder()
                .num_id(123)
                .tipo_id("Cedula").build())).thenReturn(cliente1);

        mockMvc.perform(delete("/client-service/clientsWithImages/Cedula-123").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.nombres").value(cliente1.getNombres()));
    }

    @Test
    void deleteClienteNullTest() throws Exception {
        ClientWithImageOutput cliente1 = ClientWithImageOutput.builder().num_id(123).tipo_id("Cedula")
                .nombres("AA")
                .apellidos("AA")
                .edad(20)
                .ciudad("AA").image64("Not Found").build();

        when(clientWithImageService.deleteCliente(ClienteId.builder()
                .num_id(123)
                .tipo_id("Cedula").build())).thenReturn(null);

        mockMvc.perform(delete("/client-service/clientsWithImages/Cedula-123").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }
}