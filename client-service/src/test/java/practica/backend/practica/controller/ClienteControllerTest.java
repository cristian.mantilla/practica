package practica.backend.practica.controller;

import static  org.mockito.Mockito.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import practica.backend.practica.DTO.IN.ClienteInput;
import practica.backend.practica.DTO.OUT.ClienteOutput;
import practica.backend.practica.entities.Cliente;
import practica.backend.practica.entities.ClienteId;
import practica.backend.practica.service.ClienteServiceImpl;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(ClienteController.class)
class ClienteControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ClienteServiceImpl clienteService;

    ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        objectMapper = new ObjectMapper();
    }

    @MockBean
    Cliente cliente;

    @Test
    void listClienteTest() throws Exception {
        List<ClienteOutput> clientes = new ArrayList<>();
        clientes.add(cliente.toOutput());
        when(clienteService.listAllCliente()).thenReturn(clientes);

        mockMvc.perform(get("/client-service/clientes").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    void listClienteEdadTest() throws Exception {
        List<ClienteOutput> clientes = new ArrayList<>();

        ClienteOutput cliente1 = ClienteOutput.builder().num_id(123).tipo_id("Cedula")
                .nombres("AA")
                .apellidos("AA")
                .edad(20)
                .ciudad("AA").build();
        ClienteOutput cliente2 = ClienteOutput.builder().num_id(321).tipo_id("Cedula")
                .nombres("AA")
                .apellidos("AA")
                .edad(30)
                .ciudad("AA").build();

        clientes.add(cliente1);
        clientes.add(cliente2);
        when(clienteService.listAllCliente()).thenReturn(clientes);
        when(clienteService.findAllByEdad(25)).thenReturn(clientes);

        mockMvc.perform(get("/client-service/clientes?edad=25").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    void getClienteTest() throws Exception {
        ClienteOutput cliente1 = ClienteOutput.builder().num_id(123).tipo_id("Cedula")
                .nombres("AA")
                .apellidos("AA")
                .edad(20)
                .ciudad("AA").build();

        when(clienteService.getCliente(ClienteId.builder()
                .num_id(123)
                .tipo_id("Cedula").build())).thenReturn(cliente1);

        mockMvc.perform(get("/client-service/clientes/Cedula-123").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.nombres").value(cliente1.getNombres()));
    }

    @Test
    void getClienteNullTest() throws Exception {
        when(clienteService.getCliente(ClienteId.builder()
                .num_id(123)
                .tipo_id("Cedula").build())).thenReturn(null);

        mockMvc.perform(get("/client-service/clientes/Cedula-123").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    @Test
    void createClienteTest() throws Exception {
        ClienteInput clienteInput = ClienteInput.builder().num_id(123).tipo_id("Cedula")
                .nombres("AA")
                .apellidos("AA")
                .edad(20)
                .ciudad("AA").build();

        when(clienteService.createCliente(clienteInput)).thenReturn(clienteInput.toCliente().toOutput());

        mockMvc.perform(post("/client-service/clientes").contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(clienteInput)))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.nombres").value(clienteInput.getNombres()));
    }

    @Test
    void createClienteBadRequestTest() throws Exception {
        ClienteInput clienteInput = ClienteInput.builder().num_id(123).tipo_id("Cedula")
                .nombres("AA")
                .apellidos("AA")
                .edad(0) // Edad no puede ser igual o menor a cero
                .ciudad("AA").build();

        mockMvc.perform(post("/client-service/clientes").contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(clienteInput)))
                .andExpect(status().isBadRequest());
    }

    @Test
    void updateClienteTest() throws Exception {
        ClienteInput clienteInput = ClienteInput.builder().num_id(123).tipo_id("Cedula")
                .nombres("AA")
                .apellidos("AA")
                .edad(20)
                .ciudad("AA").build();

        when(clienteService.updateCliente(clienteInput)).thenReturn(clienteInput.toCliente().toOutput());

        mockMvc.perform(put("/client-service/clientes").contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(clienteInput)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.nombres").value(clienteInput.getNombres()));
    }

    @Test
    void updateClienteNullTest() throws Exception {
        ClienteInput clienteInput = ClienteInput.builder().num_id(123).tipo_id("Cedula")
                .nombres("AA")
                .apellidos("AA")
                .edad(20)
                .ciudad("AA").build();

        when(clienteService.updateCliente(clienteInput)).thenReturn(null);

        mockMvc.perform(put("/client-service/clientes").contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(clienteInput)))
                .andExpect(status().isNoContent());
    }

    @Test
    void deleteClienteTest() throws Exception {
        ClienteOutput cliente1 = ClienteOutput.builder().num_id(123).tipo_id("Cedula")
                .nombres("AA")
                .apellidos("AA")
                .edad(20)
                .ciudad("AA").build();

        when(clienteService.deleteCliente(ClienteId.builder()
                .num_id(123)
                .tipo_id("Cedula").build())).thenReturn(cliente1);

        mockMvc.perform(delete("/client-service/clientes/Cedula-123").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.nombres").value(cliente1.getNombres()));
    }

    @Test
    void deleteClienteNullTest() throws Exception {
        ClienteOutput cliente1 = ClienteOutput.builder().num_id(123).tipo_id("Cedula")
                .nombres("AA")
                .apellidos("AA")
                .edad(20)
                .ciudad("AA").build();

        when(clienteService.deleteCliente(ClienteId.builder()
                .num_id(123)
                .tipo_id("Cedula").build())).thenReturn(null);

        mockMvc.perform(delete("/client-service/clientes/Cedula-123").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }
}