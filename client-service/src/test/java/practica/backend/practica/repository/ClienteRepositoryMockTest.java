package practica.backend.practica.repository;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import practica.backend.practica.entities.Cliente;
import practica.backend.practica.entities.ClienteId;
import practica.backend.practica.repository.ClienteRepository;

import java.util.List;

@DataJpaTest
public class ClienteRepositoryMockTest {

    @Autowired
    private ClienteRepository clienteRepository;

    @AfterEach
    void tearDown(){
        clienteRepository.deleteAll();
    }


    @Test
    public void whenFindAllByEdad_thenReturnListCliente(){
        //Given
        Cliente cliente1 = Cliente.builder()
                .clienteId(ClienteId.builder().num_id(123).tipo_id("Cedula").build())
                .nombres("AA")
                .apellidos("AA")
                .edad(20)
                .ciudad("AA").build();

        Cliente cliente2 = Cliente.builder()
                .clienteId(ClienteId.builder().num_id(321).tipo_id("Cedula").build())
                .nombres("AA")
                .apellidos("AA")
                .edad(35)
                .ciudad("AA").build();

        clienteRepository.save(cliente1);
        clienteRepository.save(cliente2);

        //When
        List<Cliente> founds = clienteRepository.findAllByEdad(25);

        //Then
        Assertions.assertThat(founds.size()).isEqualTo(1);
    }

}
