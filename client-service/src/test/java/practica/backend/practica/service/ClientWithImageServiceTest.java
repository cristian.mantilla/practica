package practica.backend.practica.service;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.multipart.MultipartFile;
import practica.backend.practica.DTO.IN.ClientImageInput;
import practica.backend.practica.DTO.IN.ClientWithImageInput;
import practica.backend.practica.DTO.IN.ClienteInput;
import practica.backend.practica.DTO.OUT.ClientImageOutput;
import practica.backend.practica.DTO.OUT.ClientWithImageOutput;
import practica.backend.practica.DTO.OUT.ClienteOutput;
import practica.backend.practica.entities.Cliente;
import practica.backend.practica.entities.ClienteId;
import practica.backend.practica.feignClient.ClientImageFeign;
import practica.backend.practica.model.ClientId;
import practica.backend.practica.repository.ClienteRepository;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;

@RunWith(SpringJUnit4ClassRunner.class)
@ExtendWith(MockitoExtension.class)
//@SpringBootTest
public class ClientWithImageServiceTest {

    @InjectMocks
    ClientWithImageServiceImpl clientWithImageService;

    @Mock
    ClienteRepository clienteRepository;

    @Mock
    ClientImageFeign clientImageFeign;

    private static final int EDAD = 25;
    private static final String TIPO = "Cedula";
    private static final int NUM = 123;
    private static final String IMG64 = "";
    private static final byte[] FILE = new byte[0];

    @Mock
    MultipartFile multipartFile;

    @Mock
    ClienteId clienteId;

    @Mock
    ClientId clientId;

    @Mock
    Cliente cliente;

    @Mock
    ClienteOutput clienteOutput;

    @Mock
    ClienteInput clienteInput;

    @Mock
    ClientWithImageOutput clientWithImageOutput;

    @Mock
    ClientWithImageInput clientWithImageInput;

    @Mock
    ClientImageOutput clientImageOutput;

    @Mock
    ResponseEntity<ClientImageOutput> clientImageOutputResponseEntity;

    @Mock
    ResponseEntity<List<ClientImageOutput>> listClientImageOutputResponseEntity;

    @Mock
    ClientImageInput clientImageInput;

    @Test
    public void listAllClienteTest(){
        List<Cliente> clientes = new ArrayList<>();

        Cliente cliente1 = Cliente.builder()
                .clienteId(ClienteId.builder().num_id(123).tipo_id("Cedula").build())
                .nombres("AA")
                .apellidos("AA")
                .edad(20)
                .ciudad("AA").build();

        clientes.add(cliente1);

        ClientId clientId = ClientId.builder().tipo_id(TIPO).num_id(NUM).build();

        //List<ClientId> clientIds = new ArrayList<>();
        //clientIds.add();

        List<ClientImageOutput> clientImageOutputs = new ArrayList<>();
        clientImageOutputs.add(ClientImageOutput.builder().clientId(clientId).image64("Not Found").build());

        Mockito.when(cliente.toImageOutput()).thenReturn(clientWithImageOutput);
        Mockito.when(clientImageFeign.getMultipleClientImage(any())).thenReturn(listClientImageOutputResponseEntity);
        Mockito.when(listClientImageOutputResponseEntity.getBody()).thenReturn(clientImageOutputs);
        Mockito.when(clientImageOutput.getImage64()).thenReturn(IMG64);
        Mockito.when(clientWithImageOutput.getTipo_id()).thenReturn(TIPO);
        Mockito.when(clientWithImageOutput.getNum_id()).thenReturn(NUM);

        Mockito.when(clienteRepository.findAll()).thenReturn(clientes);

        Assertions.assertEquals(clientes.size(), clientWithImageService.listAllCliente().size());
    }

    @Test
    public void getClienteTestClienteFound(){
        Mockito.when(clienteRepository.findById(clienteId)).thenReturn(Optional.of(cliente));
        Mockito.when(cliente.toOutput()).thenReturn(clienteOutput);
        Mockito.when(clientWithImageOutput.toCliente()).thenReturn(cliente);
        Mockito.when(cliente.getClienteId()).thenReturn(clienteId);

        Mockito.when(cliente.toImageOutput()).thenReturn(clientWithImageOutput);
        Mockito.when(clientImageFeign.getClientImage(TIPO,NUM)).thenReturn(clientImageOutputResponseEntity);
        Mockito.when(clientImageOutputResponseEntity.getBody()).thenReturn(clientImageOutput);
        Mockito.when(clientImageOutput.getImage64()).thenReturn(IMG64);
        Mockito.when(clientWithImageOutput.getTipo_id()).thenReturn(TIPO);
        Mockito.when(clientWithImageOutput.getNum_id()).thenReturn(NUM);

        Assertions.assertEquals(clienteId, clientWithImageService.getCliente(clienteId).toCliente().getClienteId());
    }

    @Test
    public void getClienteTestClienteNotFound(){
        Mockito.when(clienteRepository.findById(clienteId)).thenReturn(Optional.empty());

        Assertions.assertNull(clientWithImageService.getCliente(clienteId));
    }

    @Test
    public void createClienteTest() throws IOException {
        Mockito.when(clienteRepository.save(cliente)).thenReturn(cliente);
        Mockito.when(cliente.toInput()).thenReturn(clienteInput);
        Mockito.when(cliente.toOutput()).thenReturn(clienteOutput);
        Mockito.when(clienteOutput.toCliente()).thenReturn(cliente);
        Mockito.when(clienteInput.toCliente()).thenReturn(cliente);
        Mockito.when(cliente.getClienteId()).thenReturn(clienteId);

        Mockito.when(clientImageFeign.createClientImage(any(ClientImageInput.class))).thenReturn(clientImageOutputResponseEntity);
        Mockito.when(cliente.toImageOutput()).thenReturn(clientWithImageOutput);
        Mockito.when(clientImageOutputResponseEntity.getBody()).thenReturn(clientImageOutput);
        Mockito.when(clientImageOutput.getImage64()).thenReturn(IMG64);
        Mockito.when(clientWithImageOutput.toCliente()).thenReturn(cliente);

        Mockito.when(multipartFile.getBytes()).thenReturn(FILE);

        Assertions.assertEquals(clienteInput.toCliente().getClienteId()
                , clientWithImageService.createCliente(clienteInput,multipartFile).toCliente().getClienteId());
    }

    @Test
    public void updateClienteTestClienteFound() throws IOException {
        Mockito.when(clienteRepository.save(cliente)).thenReturn(cliente);
        Mockito.when(clienteRepository.findById(clienteId)).thenReturn(Optional.of(cliente));
        Mockito.when(cliente.toInput()).thenReturn(clienteInput);
        Mockito.when(cliente.toOutput()).thenReturn(clienteOutput);
        Mockito.when(clienteOutput.toCliente()).thenReturn(cliente);
        Mockito.when(clienteInput.toCliente()).thenReturn(cliente);
        Mockito.when(cliente.getClienteId()).thenReturn(clienteId);

        Mockito.when(clientImageFeign.updateClientImage(any(ClientImageInput.class)))
                .thenReturn(clientImageOutputResponseEntity);
        Mockito.when(cliente.toImageOutput()).thenReturn(clientWithImageOutput);
        Mockito.when(clientImageOutputResponseEntity.getBody()).thenReturn(clientImageOutput);
        Mockito.when(clientImageFeign.getClientImage(TIPO,NUM)).thenReturn(clientImageOutputResponseEntity);
        Mockito.when(clientImageOutput.getImage64()).thenReturn(IMG64);
        Mockito.when(clientWithImageOutput.getTipo_id()).thenReturn(TIPO);
        Mockito.when(clientWithImageOutput.getNum_id()).thenReturn(NUM);
        Mockito.when(clientWithImageOutput.toCliente()).thenReturn(cliente);

        Mockito.when(multipartFile.getBytes()).thenReturn(FILE);

        Assertions.assertEquals(cliente.getClienteId()
                , clientWithImageService.updateCliente(clienteInput,multipartFile).toCliente().getClienteId());
    }

    @Test
    public void updateClienteTestClienteNotFound() throws IOException {
        Mockito.when(clienteRepository.save(cliente)).thenReturn(cliente);
        Mockito.when(clienteRepository.findById(clienteId)).thenReturn(Optional.empty());
        Mockito.when(cliente.toInput()).thenReturn(clienteInput);
        Mockito.when(cliente.toOutput()).thenReturn(clienteOutput);
        Mockito.when(clienteOutput.toCliente()).thenReturn(cliente);
        Mockito.when(clienteInput.toCliente()).thenReturn(cliente);
        Mockito.when(cliente.getClienteId()).thenReturn(clienteId);

        Assertions.assertNull(clientWithImageService.updateCliente(clienteInput,multipartFile));
    }

    @Test
    public void deleteClienteTestClienteFound(){
        Mockito.when(clienteRepository.save(cliente)).thenReturn(cliente);
        Mockito.when(clienteRepository.findById(clienteId)).thenReturn(Optional.of(cliente));
        Mockito.when(cliente.toOutput()).thenReturn(clienteOutput);
        Mockito.when(clienteOutput.toCliente()).thenReturn(cliente);
        Mockito.when(cliente.getClienteId()).thenReturn(clienteId);

        Mockito.when(cliente.toImageOutput()).thenReturn(clientWithImageOutput);
        Mockito.when(clientImageFeign.getClientImage(TIPO,NUM)).thenReturn(clientImageOutputResponseEntity);
        Mockito.when(clientImageFeign.deleteClientImage(TIPO,NUM)).thenReturn(clientImageOutputResponseEntity);
        Mockito.when(clientImageOutputResponseEntity.getBody()).thenReturn(clientImageOutput);
        Mockito.when(clientImageOutput.getImage64()).thenReturn(IMG64);
        Mockito.when(clientWithImageOutput.getTipo_id()).thenReturn(TIPO);
        Mockito.when(clientWithImageOutput.getNum_id()).thenReturn(NUM);
        Mockito.when(clienteId.getTipo_id()).thenReturn(TIPO);
        Mockito.when(clienteId.getNum_id()).thenReturn(NUM);
        Mockito.when(clientWithImageOutput.toCliente()).thenReturn(cliente);

        Assertions.assertEquals(cliente.getClienteId()
                , clientWithImageService.deleteCliente(clienteId).toCliente().getClienteId());
    }

    @Test
    public void deleteClienteTestClienteNotFound(){
        Mockito.when(clienteRepository.save(cliente)).thenReturn(cliente);
        Mockito.when(clienteRepository.findById(clienteId)).thenReturn(Optional.empty());
        Mockito.when(cliente.toOutput()).thenReturn(clienteOutput);
        Mockito.when(clienteOutput.toCliente()).thenReturn(cliente);
        Mockito.when(cliente.getClienteId()).thenReturn(clienteId);

        Assertions.assertNull(clientWithImageService.deleteCliente(clienteId));
    }

    @Test
    public void findAllByEdadTest(){
        List<Cliente> clientes = new ArrayList<>();

        Cliente cliente1 = Cliente.builder()
                .clienteId(ClienteId.builder().num_id(123).tipo_id("Cedula").build())
                .nombres("AA")
                .apellidos("AA")
                .edad(20)
                .ciudad("AA").build();
        Cliente cliente2 = Cliente.builder()
                .clienteId(ClienteId.builder().num_id(321).tipo_id("Cedula").build())
                .nombres("AA")
                .apellidos("AA")
                .edad(30)
                .ciudad("AA").build();

        clientes.add(cliente1);
        clientes.add(cliente2);

        ClientId clientId = ClientId.builder().tipo_id(TIPO).num_id(NUM).build();

        //List<ClientId> clientIds = new ArrayList<>();
        //clientIds.add();

        List<ClientImageOutput> clientImageOutputs = new ArrayList<>();
        clientImageOutputs.add(ClientImageOutput.builder().clientId(clientId).image64("Not Found").build());

        Mockito.when(cliente.toImageOutput()).thenReturn(clientWithImageOutput);
        Mockito.when(clientImageFeign.getMultipleClientImage(any())).thenReturn(listClientImageOutputResponseEntity);
        Mockito.when(listClientImageOutputResponseEntity.getBody()).thenReturn(clientImageOutputs);

        Mockito.when(clienteRepository.findAllByEdad(EDAD)).thenReturn(clientes);
        Mockito.when(cliente.toImageOutput()).thenReturn(clientWithImageOutput);
        Mockito.when(clientImageOutput.getImage64()).thenReturn(IMG64);
        Mockito.when(clientWithImageOutput.getTipo_id()).thenReturn(TIPO);
        Mockito.when(clientWithImageOutput.getNum_id()).thenReturn(NUM);

        Assertions.assertEquals(clientes.size(), clientWithImageService.findAllByEdad(EDAD).size());
    }
}
