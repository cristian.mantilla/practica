package practica.backend.practica.service;

import static  org.mockito.Mockito.*;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import org.junit.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import practica.backend.practica.DTO.IN.ClienteInput;
import practica.backend.practica.DTO.OUT.ClienteOutput;
import practica.backend.practica.entities.Cliente;
import practica.backend.practica.entities.ClienteId;
import practica.backend.practica.repository.ClienteRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RunWith(SpringJUnit4ClassRunner.class)
@ExtendWith(MockitoExtension.class)
//@SpringBootTest
public class ClienteServiceTest {

    @InjectMocks
    ClienteServiceImpl clienteServiceImpl;

    @Mock
    ClienteRepository clienteRepository;

    private static final int EDAD = 25;

    @Mock
    ClienteId clienteId;

    @Mock
    Cliente cliente;

    @Mock
    ClienteOutput clienteOutput;

    @Mock
    ClienteInput clienteInput;

    @Test
    public void listAllClienteTest(){
        List<Cliente> clientes = new ArrayList<>();
        clientes.add(cliente);
        Mockito.when(clienteRepository.findAll()).thenReturn(clientes);

        Assertions.assertEquals(clientes.size(), clienteServiceImpl.listAllCliente().size());
    }

    @Test
    public void getClienteTestClienteFound(){
        Mockito.when(clienteRepository.findById(clienteId)).thenReturn(Optional.of(cliente));
        Mockito.when(cliente.toOutput()).thenReturn(clienteOutput);
        Mockito.when(clienteOutput.toCliente()).thenReturn(cliente);
        Mockito.when(cliente.getClienteId()).thenReturn(clienteId);

        Assertions.assertEquals(clienteId, clienteServiceImpl.getCliente(clienteId).toCliente().getClienteId());
    }

    @Test
    public void getClienteTestClienteNotFound(){
        Mockito.when(clienteRepository.findById(clienteId)).thenReturn(Optional.empty());

        Assertions.assertNull(clienteServiceImpl.getCliente(clienteId));
    }

    @Test
    public void createClienteTest(){
        Mockito.when(clienteRepository.save(cliente)).thenReturn(cliente);
        Mockito.when(cliente.toInput()).thenReturn(clienteInput);
        Mockito.when(cliente.toOutput()).thenReturn(clienteOutput);
        Mockito.when(clienteOutput.toCliente()).thenReturn(cliente);
        Mockito.when(clienteInput.toCliente()).thenReturn(cliente);
        Mockito.when(cliente.getClienteId()).thenReturn(clienteId);

        Assertions.assertEquals(cliente.getClienteId()
                , clienteServiceImpl.createCliente(clienteInput).toCliente().getClienteId());
    }

    @Test
    public void updateClienteTestClienteFound(){
        Mockito.when(clienteRepository.save(cliente)).thenReturn(cliente);
        Mockito.when(clienteRepository.findById(clienteId)).thenReturn(Optional.of(cliente));
        Mockito.when(cliente.toInput()).thenReturn(clienteInput);
        Mockito.when(cliente.toOutput()).thenReturn(clienteOutput);
        Mockito.when(clienteOutput.toCliente()).thenReturn(cliente);
        Mockito.when(clienteInput.toCliente()).thenReturn(cliente);
        Mockito.when(cliente.getClienteId()).thenReturn(clienteId);

        Assertions.assertEquals(cliente.getClienteId()
                , clienteServiceImpl.updateCliente(clienteInput).toCliente().getClienteId());
    }

    @Test
    public void updateClienteTestClienteNotFound(){
        Mockito.when(clienteRepository.save(cliente)).thenReturn(cliente);
        Mockito.when(clienteRepository.findById(clienteId)).thenReturn(Optional.empty());
        Mockito.when(cliente.toInput()).thenReturn(clienteInput);
        Mockito.when(cliente.toOutput()).thenReturn(clienteOutput);
        Mockito.when(clienteOutput.toCliente()).thenReturn(cliente);
        Mockito.when(clienteInput.toCliente()).thenReturn(cliente);
        Mockito.when(cliente.getClienteId()).thenReturn(clienteId);

        Assertions.assertNull(clienteServiceImpl.updateCliente(clienteInput));
    }

    @Test
    public void deleteClienteTestClienteFound(){
        Mockito.when(clienteRepository.save(cliente)).thenReturn(cliente);
        Mockito.when(clienteRepository.findById(clienteId)).thenReturn(Optional.of(cliente));
        Mockito.when(cliente.toOutput()).thenReturn(clienteOutput);
        Mockito.when(clienteOutput.toCliente()).thenReturn(cliente);
        Mockito.when(cliente.getClienteId()).thenReturn(clienteId);

        Assertions.assertEquals(cliente.getClienteId()
                , clienteServiceImpl.deleteCliente(clienteId).toCliente().getClienteId());
    }

    @Test
    public void deleteClienteTestClienteNotFound(){
        Mockito.when(clienteRepository.save(cliente)).thenReturn(cliente);
        Mockito.when(clienteRepository.findById(clienteId)).thenReturn(Optional.empty());
        Mockito.when(cliente.toOutput()).thenReturn(clienteOutput);
        Mockito.when(clienteOutput.toCliente()).thenReturn(cliente);
        Mockito.when(cliente.getClienteId()).thenReturn(clienteId);

        Assertions.assertNull(clienteServiceImpl.deleteCliente(clienteId));
    }

    @Test
    public void findAllByEdadTest(){
        List<Cliente> clientes = new ArrayList<>();

        Cliente cliente1 = Cliente.builder()
                .clienteId(ClienteId.builder().num_id(123).tipo_id("Cedula").build())
                .nombres("AA")
                .apellidos("AA")
                .edad(20)
                .ciudad("AA").build();
        Cliente cliente2 = Cliente.builder()
                .clienteId(ClienteId.builder().num_id(321).tipo_id("Cedula").build())
                .nombres("AA")
                .apellidos("AA")
                .edad(30)
                .ciudad("AA").build();

        clientes.add(cliente1);
        clientes.add(cliente2);

        Mockito.when(clienteRepository.findAllByEdad(EDAD)).thenReturn(clientes);

        Assertions.assertEquals(clientes.size(), clienteServiceImpl.findAllByEdad(EDAD).size());
    }
}
