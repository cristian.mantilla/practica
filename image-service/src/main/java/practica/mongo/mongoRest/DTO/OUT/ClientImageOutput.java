package practica.mongo.mongoRest.DTO.OUT;

import lombok.*;
import practica.mongo.mongoRest.document.ClientId;
import practica.mongo.mongoRest.document.ClientImage;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ClientImageOutput implements Serializable {

    private ClientId clientId;
    private String image64;

    public ClientImage toClientImage(){
        return ClientImage.builder()
                .clientId(ClientId.builder()
                        .tipo_id(getClientId().getTipo_id())
                        .num_id(getClientId().getNum_id()).build())
                .image64(getImage64()).build();
    }

}
