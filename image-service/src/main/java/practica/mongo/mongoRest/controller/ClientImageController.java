package practica.mongo.mongoRest.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import practica.mongo.mongoRest.DTO.IN.ClientImageInput;
import practica.mongo.mongoRest.DTO.OUT.ClientImageOutput;
import practica.mongo.mongoRest.document.ClientId;
import practica.mongo.mongoRest.document.ClientImage;
import practica.mongo.mongoRest.service.ClientImageService;

import javax.validation.Valid;
import java.io.IOException;
import java.util.Base64;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/images")
public class ClientImageController {

    @Autowired
    private ClientImageService clientImageService;

    @GetMapping
    public ResponseEntity<List<ClientImageOutput>> listClientImage (){
        List<ClientImageOutput> imagesOutput = clientImageService.listAllClientImage();
        if (imagesOutput.isEmpty()){
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(imagesOutput);
    }

    @GetMapping(value = "/{tipo}-{id}")
    public ResponseEntity<ClientImageOutput> getClientImage(@PathVariable("tipo") String tipo,@PathVariable("id") int id){
        ClientId clientId = ClientId.builder().num_id(id).tipo_id(tipo).build();
        log.info("Fetching ClientImage with id {}", clientId);
        ClientImageOutput cliente = clientImageService.getClientImage(clientId);

        if(null == cliente){
            log.error("ClientImage with id {} not found.", clientId);
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(cliente);
    }

    @PostMapping("/multiple")
    public ResponseEntity<List<ClientImageOutput>> getMultipleClientImage(@RequestBody List<ClientId> clientsId) {
        return ResponseEntity.ok(clientImageService.listAllClientImageByClientId(clientsId));
    }

    @PostMapping()
    public ResponseEntity<ClientImageOutput> createClientImage (@RequestBody ClientImageInput clientImageInput)
            throws IOException {
        //ClientId clientId = ClientId.builder().tipo_id(tipo).num_id(num).build();
        ClientImageOutput clientImage =
                clientImageService.createClientImage(clientImageInput.getClientId(), clientImageInput.getImage64());
        log.info("Creating ClientImage : {}", clientImage);
        return ResponseEntity.status(HttpStatus.CREATED).body(clientImage);
    }

    @PutMapping()
    public ResponseEntity<ClientImageOutput> updateClientImage (@RequestBody ClientImageInput clientImageInput)
            throws IOException {

        log.info("Updating ClientImage with id {}", clientImageInput.getClientId());
        ClientImageOutput clientImage =
                clientImageService.updateClientImage(clientImageInput.getClientId(),clientImageInput.getImage64());

        if(clientImage == null){
            log.error("Unable to update. Cliente with id {} not found.", clientImageInput.getClientId());
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(clientImage);
    }

    @PostMapping("/file")
    public ResponseEntity<ClientImageOutput> createClientImageFile
            (@RequestPart ClientId clientId, @RequestPart MultipartFile multipartFile)
            throws IOException {
        //ClientId clientId = ClientId.builder().tipo_id(tipo).num_id(num).build();
        ClientImageOutput clientImage = clientImageService
                .createClientImage(clientId, Base64.getEncoder().encodeToString(multipartFile.getBytes()));
        log.info("Creating ClientImage : {}", clientImage);
        return ResponseEntity.status(HttpStatus.CREATED).body(clientImage);
    }

    @PutMapping("/file")
    public ResponseEntity<ClientImageOutput> updateClientImageFile
            (@RequestPart ClientId clientId, @RequestPart MultipartFile multipartFile)
            throws IOException {

        log.info("Updating ClientImage with id {}", clientId);
        ClientImageOutput clientImage = clientImageService
                .updateClientImage(clientId,Base64.getEncoder().encodeToString(multipartFile.getBytes()));

        if(clientImage == null){
            log.error("Unable to update. Cliente with id {} not found.", clientId);
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(clientImage);
    }

    @DeleteMapping(value = "/{tipo}-{id}")
    public ResponseEntity<ClientImageOutput> deleteClientImage
            (@PathVariable("tipo") String tipo,@PathVariable("id") int id){

        ClientId clientId = ClientId.builder().num_id(id).tipo_id(tipo).build();
        log.info("Fetching & Deleting ClientImage with id {}", clientId);
        ClientImageOutput cliente1 = clientImageService.deleteClientImage(clientId);

        if(cliente1 == null){
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(cliente1);
    }

}
