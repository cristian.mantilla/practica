package practica.mongo.mongoRest.document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.FieldType;
import org.springframework.data.mongodb.core.mapping.MongoId;
import practica.mongo.mongoRest.DTO.IN.ClientImageInput;
import practica.mongo.mongoRest.DTO.OUT.ClientImageOutput;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "clientImages")
@Builder
public class ClientImage {

    @Id
    private ClientId clientId;

    @Field
    private String image64;

    public ClientImageInput toInput(){
        return ClientImageInput.builder()
                .clientId(ClientId.builder()
                        .tipo_id(getClientId().getTipo_id())
                        .num_id(getClientId().getNum_id()).build())
                .image64(getImage64()).build();
    }

    public ClientImageOutput toOutput(){
        return ClientImageOutput.builder()
                .clientId(ClientId.builder()
                        .tipo_id(getClientId().getTipo_id())
                        .num_id(getClientId().getNum_id()).build())
                .image64(getImage64()).build();
    }


}
