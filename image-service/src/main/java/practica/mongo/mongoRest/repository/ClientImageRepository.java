package practica.mongo.mongoRest.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import practica.mongo.mongoRest.document.ClientId;
import practica.mongo.mongoRest.document.ClientImage;

public interface ClientImageRepository extends MongoRepository<ClientImage, ClientId> {

}
