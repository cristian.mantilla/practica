package practica.mongo.mongoRest.service;

import org.springframework.web.multipart.MultipartFile;
import practica.mongo.mongoRest.DTO.IN.ClientImageInput;
import practica.mongo.mongoRest.DTO.OUT.ClientImageOutput;
import practica.mongo.mongoRest.document.ClientId;

import java.io.IOException;
import java.util.List;

public interface ClientImageService {

    public List<ClientImageOutput> listAllClientImage();

    public List<ClientImageOutput> listAllClientImageByClientId(List<ClientId> clientIds);

    public ClientImageOutput getClientImage(ClientId clientId);

    public ClientImageOutput createClientImage(ClientId clientId, String file64);
    public ClientImageOutput updateClientImage(ClientId clientId, String file64);

    public ClientImageOutput deleteClientImage(ClientId clientId);

}
