package practica.mongo.mongoRest.service;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import practica.mongo.mongoRest.DTO.IN.ClientImageInput;
import practica.mongo.mongoRest.DTO.OUT.ClientImageOutput;
import practica.mongo.mongoRest.document.ClientId;
import practica.mongo.mongoRest.document.ClientImage;
import practica.mongo.mongoRest.repository.ClientImageRepository;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Optional;

@Service
public class ClientImageServiceImpl implements ClientImageService {

    @Autowired
    private ClientImageRepository clientImageRepository;


    @Override
    public List<ClientImageOutput> listAllClientImage() {
        return toOutputList(clientImageRepository.findAll());
    }

    @Override
    public List<ClientImageOutput> listAllClientImageByClientId(List<ClientId> clientIds) {
        List<ClientImageOutput> images = new ArrayList<>();
        for (ClientId id : clientIds){
            ClientImageOutput clientImage = getClientImage(id);
            images.add(clientImage);
        }
        return images;
    }

    @Override
    public ClientImageOutput getClientImage(ClientId clientId) {
        return clientImageRepository.findById(clientId).orElse(null).toOutput();
    }

    @Override
    public ClientImageOutput createClientImage(ClientId clientId, String file64) {
        ClientImage clientImage = ClientImage.builder()
                .clientId(clientId)
                .image64(file64).build();
        return clientImageRepository.save(clientImage).toOutput();
    }

    @Override
    public ClientImageOutput updateClientImage(ClientId clientId, String file64) {

        ClientImage clientImage = getClientImage(clientId).toClientImage();

        if (null == clientImage){
            return null;
        }

        clientImage.setClientId(clientId);
        clientImage.setImage64(file64);

        return clientImageRepository.save(clientImage).toOutput();
    }

    @Override
    public ClientImageOutput deleteClientImage(ClientId clientId) {

        ClientImage clientImage = getClientImage(clientId).toClientImage();

        if (null == clientImage){
            return null;
        }

        clientImageRepository.delete(clientImage);
        return clientImage.toOutput();
    }

    public List<ClientImageOutput> toOutputList(List<ClientImage> images){
        List<ClientImageOutput>  imagesOut = new ArrayList<>();
        for(ClientImage imagen : images) {
            imagesOut.add(imagen.toOutput());
        }
        return imagesOut;
    }

}
